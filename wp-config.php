<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-parallax-one' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '12345' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'XEA:O[Kvgn}8BcJm*O(D0Cpj*,`wQN{`7zDu&5y>37!*;T#WZ3l9c<rv$^AsTy.1' );
define( 'SECURE_AUTH_KEY',  'I[!_i;WnomF%Q{zeU/f!tU,-6!XE)7F_OM[Q+cxNWkQyrS-;pk+fKiTd5#ktomw<' );
define( 'LOGGED_IN_KEY',    'WXKNxcF|)FictcZ9IzIK2%?j;g/%yEn[Gwp=>LwWssHrMT-~(UwmFsJCY}dAaR:T' );
define( 'NONCE_KEY',        '=4r4NJ`u30E&g~Oea8O>&>7H9W_E9eOt!PL.$z}H9^=6L RtHxX*aZ-0uG*8t;>(' );
define( 'AUTH_SALT',        '2&3lP@7RkJfak8*~s#;*?^ 88POJqGhVIy<aDX8gG*>5hfEKbivgFatg?{<>U0RQ' );
define( 'SECURE_AUTH_SALT', 'uP!1#{X`RjBS#yq32Sw<X}Jd9o]@~^U.di2O^|#x*`@iQ>{AV*kB%Ltw.Aoop44^' );
define( 'LOGGED_IN_SALT',   '!sX|J@kYYL%--eMSQ-7al/iEa@a=Z A{o^f`3K<&[,sWpsG{V4K3!dT/_>XZg0>v' );
define( 'NONCE_SALT',       'b% z6w~qw9)JQ;Nc@IfEfPdVYicU4n+fktk9ME>vm|XzpX:QgTP9 [G=IfB5t@m[' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
